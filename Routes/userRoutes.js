const express = require('express')
const router= express.Router();
const userController = require("../controllers/UserControllers")
// const {checkEmailExists,registerUser,userController}= userController;
// const cors = require('cors');


// router.get('/', );
router.post("/checkemail", userController.checkEmailExists);
router.post("/register", userController.registerUser);
router.post("/login", userController.loginUser);
router.post("/getUser", userController.getUserInfos);

module.exports = router;