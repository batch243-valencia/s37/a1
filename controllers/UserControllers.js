const User = require('../models/Users')
const bcrypt = require("bcrypt");
const { response } = require('express');


// check email if already exist
checkEmailExists = async (req,res) => {


    const result = await User.find({ email: req.body.email });
    console.log(req.body.email);
    let message_1 = "";
    if (result.length > 0) {
        message_1 = `The ${req.body.email} is already been taken, please use other email.`;
        return res.send(message_1);
    } else {
        message_1 = `The ${req.body.email} is not taken.`;
        return res.send(message_1);
    }
}

registerUser = (req,res)=> {
    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: bcrypt.hashSync(req.body.password, 10),
        mobileNo: req.body.mobileNo
       
    })
    return newUser.save().then(user=>{
        console.log(user)
         res.send(`Congratulation ${newUser.firstName}`)
    }).catch(err=>{
        console.log(err);
        res.send(`Sorry ${newUser.firstName} there was an  error during the registration`)
    })
}

loginUser =(req,res)=>{
  return User.findOne({email: req.body.email})
  .then(result =>{
    // console.log(result);
    if (result === null) {
        res.send(`Your email :${req.body.email}, is not yet registered. register first`)
    }else{
        const isPasswordCOrrect = bcrypt.compareSync(req.body.password,result.password);
        if(isPasswordCOrrect){
            return res.send(`login succesfully`)
        }else{
            return res.send(`email password mismatch, please try again!`)
        }
    } 
  })
}

getUserInfos =(req,res)=>{
 return User.findOne({ "_id": req.body._id }).then(result =>{
            {result.password=""}
            return res.send(result)
        
    }).catch( err=>
        {
        return res.status(404).json({error: 'User Doesnt exist'})
        }
    )
}


module.exports = {checkEmailExists, registerUser, loginUser, getUserInfos}